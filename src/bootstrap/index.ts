declare let __DEV__: boolean;

const registerSW = async (): Promise<void> => {
  if (__DEV__ || navigator.serviceWorker === undefined) return;

  const onSwUpdated = (): void => {
    // TODO: Show as a notification with DOM
    console.info('Content updated!');
  };

  const onSwCached = (): void => {
    // TODO: Show as a notification with DOM
    console.info('Content cached for offline use!');
  };

  // Register the service worker
  try {
    const registration = await navigator.serviceWorker.register('../service-worker.js');
    registration.onupdatefound = () => {
      const installingWorker = registration.installing;
      if (!installingWorker) return;

      installingWorker.onstatechange = () => {
        if (installingWorker.state !== 'installed') return;

        if (navigator.serviceWorker.controller) {
          onSwUpdated();
          return;
        }

        onSwCached();
      };
    };
  } catch (e) {
    // Nothing to do
  }
};

const hideSplash = (): void => {
  const splash = document.getElementById('splash');
  if (splash) document.body.removeChild(splash);
};

const loadGame = async (): Promise<void> => {
  // Lazily load the game app
  const { default: GameApp } = await import(/* webpackChunkName: "game" */ 'game/app');

  const container = document.getElementById('game');
  if (!container) {
    alert('Could not find container element!');
    return;
  }

  console.log('game loaded', GameApp);
  hideSplash();
  registerSW();
};

if (document.readyState === 'complete') {
  loadGame();
} else {
  document.onreadystatechange = () => {
    if (document.readyState !== 'complete') return;
    loadGame();
  };
}
