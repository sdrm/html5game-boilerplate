const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const WorkboxPlugin = require('workbox-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FaviconWebpackPlugin = require('favicons-webpack-plugin')

const metadata = require('./metadata')

module.exports = (_, argv) => {
  const mode = argv.mode || 'production'
  const isProd = (mode === 'production')

  const fileLoader = {
    loader: 'file-loader',
    options: {
      name() {
        return isProd
          ? 'assets/[sha512:hash:base64:7].[ext]'
          : '[path][name].[ext]'
      },
    },
  }

  const tsLoader = {
    loader: 'ts-loader',
  }

  // Common config
  const config = {
    // Development or Production mode
    mode,
    // Handle bootstrap script alone
    entry: {
      bootstrap: './src/bootstrap/index.ts',
    },
    // Tell webpack to use typescript loader
    module: {
      rules: [{
        test: /\.css$/i,
        use: ['css-loader'],
      }, {
        test: /\.(ico|webp|svg|png|jpg|gif|mp3|woff|woff2)$/i,
        use: fileLoader,
      }, {
        test: /\.ts$/i,
        use: tsLoader,
      }],
    },
    // Resolve .ts then .js files when extension is not supplied
    // Resolve files in 'src' directory
    resolve: {
      extensions: ['.ts', '.js'],
      modules: ['./src', 'node_modules'],
    },
    // Output files as dist/runtime/<module/chunk name>.js
    output: {
      filename: 'runtime/[chunkhash:7].js',
      chunkFilename: 'runtime/[chunkhash:7].js',
      path: path.resolve(__dirname, 'dist'),
    },
    // Use only one chunk for runtime,
    // Use *webpackChunkName* comments when separate chunks are needed
    // https://webpack.js.org/guides/code-splitting/#dynamic-imports
    optimization: {
      runtimeChunk: 'single',
    },
    // Load some webpack plugins
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.HashedModuleIdsPlugin(),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'src/templates/index.html.ejs',
        minify: {
          collapseWhitespace: isProd,
          minifyJS: isProd,
          minifyCSS: isProd && { level: 2 },
        },
        templateParameters: {
          metadata,
        },
      }),
      new FaviconWebpackPlugin({
        logo: './src/assets/icon.svg',
        publicPath: './',
        prefix: '',
        inject(htmlPlugin) {
          return htmlPlugin.options.filename === 'index.html'
        },
        favicons: metadata,
      }),
      new webpack.DefinePlugin({
        __DEV__: isProd ? 'false' : 'true',
      }),
    ],
  }

  if (isProd) {
    // Production-only flags
    Object.assign(config, {
      optimization: {
        ...config.optimization,
        splitChunks: {
          cacheGroups: {
            vendors: {
              test: /[\\/]node_modules[\\/]/,
              name: 'vendors',
              chunks: 'all',
            },
          },
        },
        minimizer: [
          new TerserPlugin({
            parallel: true,
            terserOptions: {
              compress: {
                'drop_console': true,
              },
              output: {
                comments: false,
              },
            },
          }),
        ],
      },
      plugins: [
        ...config.plugins,
        new WorkboxPlugin.GenerateSW({
          clientsClaim: true,
          skipWaiting: true,
          importWorkboxFrom: 'local',
        }),
      ],
    })
  } else {
    // Dev-only config
    Object.assign(config, {
      devtool: 'inline-source-map',
      watch: true,
      devServer: {
        contentBase: './dist',
      },
      output: {
        ...config.output,
        filename: 'runtime/[name].js',
        chunkFilename: 'runtime/[name].js',
      },
    })

    tsLoader.options = {
      transpileOnly: true,
      experimentalWatchApi: true,
    }
  }

  return config
}
