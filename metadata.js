// See: https://github.com/itgalaxy/favicons#usage
module.exports = {
  appName: 'Base Game',
  appShortName: 'Base Game',
  appDescription: "Base game template",
  'theme_color': '#000000',
  background: '#000000',
  'start_url': '/?utm_source=a2hs',
  gAnalyticsID: process.env['GANALYTICS_ID'],
}
